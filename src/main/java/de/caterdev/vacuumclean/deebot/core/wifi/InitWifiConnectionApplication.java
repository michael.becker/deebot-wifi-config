/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2020, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.core.wifi;

import de.caterdev.vacuumclean.deebot.core.DataParseUtils;
import org.apache.commons.cli.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Base64;
import java.util.Scanner;

/**
 * Application to set the wifi settings of the Deebot Ozmo 930.
 * <p>
 * Necessary steps:
 * <ul>
 *     <li>Reset the robot</li>
 *     <li>Connect to the WiFi Ecovacs_XXXX</li>
 *     <li>run this application using command line arguments or in interactive mode</li>
 * </ul>
 */
public class InitWifiConnectionApplication {
    private static final Options options = new Options();
    private static final Option ssidOption = new Option("s", "ssid", true, "WiFi SSID");
    private static final Option passphraseOption = new Option("p", "passphrase", true, "WiFi passphrase");
    private static final Option adminUserOption = new Option("u", "adminUser", true, "username of deebot admin");
    private static final Option hostOption = new Option("h", "host", true, "IP address of the host running the deebot server");

    static {
        options.addOption(ssidOption);
        options.addOption(passphraseOption);
        options.addOption(adminUserOption);
        options.addOption(hostOption);
    }


    public static void main(String[] args) throws Exception {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);
        Scanner in = new Scanner(System.in);

        String ssid;
        String passphrase;
        String adminUser;
        String host;

        if (cmd.hasOption(ssidOption.getOpt())) {
            ssid = cmd.getOptionValue(ssidOption.getOpt());
        } else {
            System.out.println("SSID: ");
            ssid = in.nextLine();
        }

        if (cmd.hasOption(passphraseOption.getOpt())) {
            passphrase = cmd.getOptionValue(passphraseOption.getOpt());
        } else {
            System.out.println("Passphrase: ");
            passphrase = in.nextLine();
        }

        if (cmd.hasOption(adminUserOption.getOpt())) {
            adminUser = cmd.getOptionValue(adminUserOption.getOpt());
        } else {
            System.out.println("Admin user: ");
            adminUser = in.nextLine();
        }

        if (cmd.hasOption(hostOption.getOpt())) {
            host = cmd.getOptionValue(hostOption.getOpt());
        } else {
            System.out.println("Host IP: ");
            host = in.nextLine();
        }

        connectRobot(ssid, passphrase, adminUser, host);
    }

    static void connectRobot(String ssid, String passphrase, String adminUser, String hostIP) throws Exception {
        Socket socket = new Socket("192.168.0.1", 9876);
        socket.setKeepAlive(true);
        System.out.println(socket.isConnected());

        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        String encrypt = "yes";
        String appendInfo = String.valueOf(ssid.getBytes().length + passphrase.getBytes().length);
        String slkParam = String.format("<ctl td='SetSlkParam' user='%s' lb='%s'/>", adminUser, hostIP);
        String slkMessage = Base64.getEncoder().encodeToString(DataParseUtils.getConfigBuffer(slkParam));

        String message = String.format(message = "@{\"td\":\"scpa\",\"ssid\":\"%s\",\"passphrase\":\"%s\",\"encrypt\":\"%s\",\"append_info\":\"%s\",\"slk_msg\":\"%s\"}\00", ssid, passphrase, encrypt, appendInfo, slkMessage);


        System.out.println(message);
        out.write(message);
        out.flush();
        System.out.println(socket.getKeepAlive());

        String data = in.readLine();
        System.out.println(data);

        int messageBegin = data.indexOf("resp_msg") + 11;
        int messageEnd = data.length() - 3;
        String responseMessage = data.substring(messageBegin, messageEnd);
        byte[] decoded = Base64.getDecoder().decode(responseMessage);
        System.out.println("resp_msg:\n" + new String(decoded));

        in.close();
        out.close();
        socket.close();
    }
}
